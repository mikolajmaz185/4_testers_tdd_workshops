"""Napisz klasę Airline, która opisuje linie lotnicze. Linia lotnicza w
trakcie inicjalizacji przyjmuje listę samolotów. Napisz w klasie
metodę zwracającą ilość wszystkich dostępnych miejsc we
wszystkich samolotach."""


class Airplane:

    def __init__(self, name, flown_distance, seats_in_airplane, taken_seats):
        self.name = name
        self.flown_distance = 0
        self.seats_in_airplane = seats_in_airplane
        self.taken_seats = 0

    def fly(self, distance):
        self.flown_distance += distance

    def is_service_required(self):

        if self.flown_distance >= 10000:
            return True
        else:
            return False

    def board_passenger(self,number_of_passangers):

        if self.taken_seats + number_of_passangers <= self.seats_in_airplane:
            self.taken_seats += number_of_passangers
        else:
            self.taken_seats = self.seats_in_airplane

    def get_available_seats(self):
        available_seats = self.seats_in_airplane - self.taken_seats
        if available_seats < 0:
            available_seats = 0
            return available_seats
        else:
            return available_seats


if __name__ == '__main__':

    airbus737 = Airplane("Airbus 737", 0, 220, 0)
    dreamliner787 = Airplane("Airbus 787", 0, 386, 0)

    # fly
    airbus737.fly(3000)
    print(airbus737.is_service_required())
    airbus737.fly(5000)
    print(airbus737.flown_distance)
    print(airbus737.is_service_required())
    airbus737.fly(5000)
    print(airbus737.flown_distance)
    print(airbus737.is_service_required())

    # boarding plane
    airbus737.board_passenger(15)
    print(f"First boarding {15}")
    print(f"Taken seats {airbus737.taken_seats}")
    print(f"Available seats: {airbus737.get_available_seats()}")
    print("--------------------")
    airbus737.board_passenger(115)
    print(f"Second boarding {115}")
    print(f"Taken seats {airbus737.taken_seats}")
    print(f"Available seats: {airbus737.get_available_seats()}")
    print("--------------------")
    airbus737.board_passenger(95)
    print(f"Third boarding {95}")
    print(f"Taken seats {airbus737.taken_seats}")
    print(f"Available seats: {airbus737.get_available_seats()}")
