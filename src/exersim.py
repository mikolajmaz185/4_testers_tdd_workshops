

# TODO: define the 'EXPECTED_BAKE_TIME' constant.

EXPECTED_BAKE_TIME = 40

# elapse - upłynąć
# remain - pozostały

# TODO: Remove 'pass' and complete the 'bake_time_remaining()' function below


def bake_time_remaining(elapsed_bake_time):

    remaining_bake_time = EXPECTED_BAKE_TIME - elapsed_bake_time

    return remaining_bake_time


# TODO: Define the 'preparation_time_in_minutes()' function below.
# You might also consider using 'PREPARATION_TIME' here, if you have it defined.

def preparation_time_in_minutes(layer):
    return layer * 2


# TODO: define the 'elapsed_time_in_minutes()' function below.
# Remember to add a docstring (you can copy and then alter the one from bake_time_remaining.)

def elapsed_time_in_minutes(number_of_layers, elapsed_bake_time):
    """Calculate the bake time remaining.

    :param elapsed_bake_time: int - baking time already elapsed.
    :return: int - remaining bake time (in minutes) derived from 'EXPECTED_BAKE_TIME'.

    Function that takes the actual minutes the lasagna has been in the oven as
    an argument and returns how many minutes the lasagna still needs to bake
    based on the `EXPECTED_BAKE_TIME`.
    """

    remaining_time = preparation_time_in_minutes(number_of_layers) + elapsed_bake_time
    return remaining_time


if __name__ == '__main__':
    print(elapsed_time_in_minutes(3, 20))

