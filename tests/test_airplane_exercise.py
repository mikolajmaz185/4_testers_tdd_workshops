from src.airplane_exercise import Airplane


def test_airplane_features():

    boeing787 = Airplane("Boeing787", 0, 286, 0)
    assert boeing787.name == "Boeing787"
    assert boeing787.seats_in_airplane == 286
